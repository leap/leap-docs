---
title: Circumvention
geekdocNav: true
geekdocAnchor: true
---

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD033 -->

<!-- markdownlint-restore -->

An overview of censorship techniques, and some circumvention tactics used by LEAP.

{{< toc >}}

## Introduction

We understand that if you _are_ already using a VPN, you already are concerned
about surveillance and censorship, and how these two things are usually two
sides of the same coin. The Electronic Frontier Foundation maintains
the excellent [Understanding and Circumventing Network
Censorship](https://ssd.eff.org/module/understanding-and-circumventing-network-censorship)
as part of their surveillance self-defense guide.

Even if you already know stuff about censorship, we invite you to refresh you
knowledge in that resource! To understand the ever-evolving game of censorship
circumvention, having a good mental model of where the blocking can happen
helps a lot to design evasion techniques - or understand how do they work, and
how to help investigate when the situation changes with time.

If you are into dead trees, or their electronic equivalent, there's this little
book by [ARTICLE 19](https://www.article19.org/) that we cannot praise enough: [How the Internet Really Works - An Illustrated Guide to Protocols, Privacy, Censorship, and Governance](https://nostarch.com/how-internet-really-works). Circumvention is
covered in there, and you'll end up with a rich imagery about how the internet
works (and it has some cat illustrations too!).

## Different modalities of internet interference

### Who can censor the internet?

Blocking or filtering can be done by your **local network administrators**,
  or by your Internet Service Provider (**ISP**). Many times ISPs are compelled
  by governments to enact blocking or filtering.

### What is the target of the block?

Blocks (on websites) can happen at many levels: two common examples are blocks
at the level of **IP Address**, or by messing with the **DNS** (Domain Name
System: that thing that allows you to type funny names in the browser bar
instead of very long series of numbers - and the same thing that forces you to
buy cool domain names for your new project or campaign).

Depending on how access to content is interfered with, we can consider several categories of blocks:

### 1. Blocking a website by IP

Access to a certain IP address is denied. If multiple websites share the same IP (for example, if they are hosted at the same machine), all of these websites are inaccessible.

### 2. Blocking a website by DNS

Access to the website is denied by changing the domain name system (DNS) records so that the domain name of the website is not resolved to its IP address.

### 3. Bandwidth throttling

Internet Service Providers can slows down the speed of certain websites, making it difficult to use them.

### 4. Filtering by protocol

Internet Service Providers blocks certain types of internet traffic, such as file downloads, anonymizers and Virtual Private Networks, based on protocol used, such as OpenVPN.

### 5. Filtering by Deep Packet Inspection: 

Even encrypted traffic can be fingerprinted by meta-parameters such as packet length and by unencrypted header content, if any. Connections considered suspicious are blocked.

### 6. Content removal

Government pressures content owners or platforms to remove content from the web. 

### 7. Network disruptions

Government disrupts the normal functioning of the internet. This may include denial of service (DoS) attacks or forcing internet providers to temporarily suspend operation in certain areas.

## Censorship in the broader context

Censorship is a complex topic, and we can approach it from many angles. [Access
Now elaborated a Taxonomy of Internet Shutdowns](https://www.accessnow.org/publication/internet-shutdown-types/) that
partially matches the classification we offered above. According to it, internet
interference can go from a total Internet Infrastructure blackout (or shutdown) to Routing Disruption, DNS
manipulation, Selective Filtering, Deep Packet Inspection, Rogue Infrastructure
Attacks, Denial of Service or Throttling.

## How does a VPN Help With Censorship?

* A VPN works to circumvent "regular" website blocks, but then ISPs or
  Governments turn to detect and block VPNs too. It is a quite tiresome game.

* A VPN usually does a few steps to get you connected, and all those steps are
  potential targets that governments and ISPs can try to block - and for which
  we have some countermeasures too.

* Governments or ISPs generally try to block VPNs on 4 levels:
  1. **Distribution sites** for the app (like, shutting down the app store),
  2. Blocking access to the **initial discovery** and configuration of gateways,
  3. Blocking access to the **VPN gateways** themselves;
  4. Not doing any of the above, but letting the connection happen and then
    **degrading the connection** till the point in which everything is painfully
    slow and then the user (not you!) gets tired and frustrated and bored and
    then swears to never use a VPN again. This is commonly known as
    "throttling" - and detecting it with enough empirical evidence proves to be
    a particularly hard question.

* Some of the attempts to "block" access to VPN are easier to counter (or the
  VPN Provider side) than others.

* Some VPN **protocols** are easier to fingerprint (that's it, to detect) than
  others. Vanilla OpenVPN (yeah, what we use for the regular gateways) is
  particularly easy to block.

* The good news is that there are some protocols that are know to resist common
  detection techniques - at least for some time.

* Even if you're lucky and you manage to get a VPN tunnel working, the websites
  you are visiting can also detect that you're using a VPN, and decide not to
  show you their content. They usually do this by paying third parties for
  lists that gather common IP ranges for known VPN gateways. This is a case of
  what is known as **geofencing** (a type of server-side blocking).


### 1. When a website is filtered (or slowed down) by IP

When user accesses a website with LEAP VPN, the Internet Service Provider
cannot see which particular website the user is accessing. From ISP's point of
view, user's computer talks to an unrelated machine in another country.

For **regular gateways**, even if the particular website is not visible, the ISP
can detect the use of OpenVPN and try to throttle or block the VPN connection
itself.

When using **bridges**, the tunnel is obfuscated until the gateway, and we
assume that the VPN gateway has unfiltered access to the internet.


### 2. When a website is blocked by DNS

LEAP VPN routes all applications' DNS requests through the VPN gateway. Since
the connection to the Gateway is encrypted, DNS requests cannot be seen or
blocked by ISP.

### 3. Filtering by protocol and by Deep Packet Inspection

ISPs can block OpenVPN connections, and try to enumerate gateways to block them by IP.

This is why we recommend to use bridges to help access gateways from censored environments.
Bridges use the [obfs4
protocol](https://github.com/Yawning/obfs4/blob/master/doc/obfs4-spec.txt) to
wrap and conceal OpenVPN data transfers between the user and a
bridge. From outside, the obfs4 tunnel looks like an exchange of random noise,
which in theory helps passing under the censors's radar (but, highly entropic
traffic has its problems).

We are also working on non-public bridges, which should be harder to enumerate,
and thus less likely to be blocked.

### 4. Content removal and service-oriented network disruptions

A VPN cannot really help with those, but can be used in combination with other
tools. One may want to use censorship-resistant decentralised platforms like Fediverse
to be less affected by content removal. For more resilient internet service,
one may consider satellite internet, stationary mesh networks like Freifunk if
such exist in one's area, mobile mesh networks, or using smaller ISPs or combination of ISPs.

## How to make your VPN usage more Censorship-Resistant

There are a few techniques that can be used with LEAP VPN that help to counter
some of the blockings mentioned above. Some of these will be attempted
automatically, others will need the user to change application settings.

### During the initial phase (tunnel bootstrapping)

* If we fail to fetch the initial configuration, LEAP VPN Clients will try to fetch using Snowflake (needs Tor installed).
* The TLS fingerprint used by the desktop libraries can show that we're not a browser. We are enabling uTLS to simulate to be a browser (and be less conspicuous in the mass of network traces).

### Using bridges to connect to the OpenVPN Gateway

What we call **bridges** are obfuscated proxies that relay the traffic until
the final destination at the gateway. Using bridges can avoid that the ISP
detects that the user is establishing a VPN tunnel.

By the way, we borrowed the term "bridge" from the Tor community, hoping that
the use of a known metaphor makes the same idea easier to
translate.

We're using two pluggable transports, with different configuration options:

    1. obfs4
    2. obfs4 in KCP mode
    3. obfs4 in hopping mode. 

You can refer to the [Hopping Transport]({{<ref "/circumvention/hopping" >}}) page for more technical information about how the hopping mode works.

## Monitoring

You don't have to be an expert or an academic person to contribute to
censorship research (yes, those two things usually don't are automatically
equivalent). In fact, some of the most valuable findings about what different
censors are using, and what remain to be viable circumvention
techniques, are done by anonymous persons that volunteer their time and skills
to advance our collective knowledge.

If this sounds something that you'd be interested in doing, please read carefully on
the page about [Monitoring VPN with the Open Observatory for Network Interference]({{<ref "/circumvention/ooni" >}}).
