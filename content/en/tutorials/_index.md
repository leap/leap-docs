---
title: Tutorials
geekdocNav: true
geekdocAnchor: true
---

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD033 -->

<!-- markdownlint-restore -->

Goal-oriented, step-by-step writeups. Their aim is to get you familiarized with
the building blocks of the LEAP VPN Platform, guiding you step by step while
also explaining how components connect to each other.

## Setting up the Platform

* [Lilypad Readme](https://0xacab.org/leap/container-platform/lilypad/-/blob/main/README.md): it covers the basics of setting up the platform.


## Censorship Circumvention

* [Setting Up An Obfuscated OpenVPN Gateway, Part 1]({{< ref "/tutorials/obfsvpn-part-1" >}}): it shows how to manually setup your own obfuscated gateway with [obfsvpn](https://0xacab.org/leap/obfsvpn) on a commodity VPS.
* [Setting Up An Obfuscated OpenVPN Gateway, Part 2]({{< ref "/tutorials/obfsvpn-part-2-hopping" >}}): building from the previous part, it enables the [Hopping Pluggable Transport]({{< ref "/circumvention/hopping" >}})

