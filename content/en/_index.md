+++
title = "Welcome to the LEAP documentation!"
geekdocNav = true
geekdocAlign = "left"
geekdocAnchor = false
+++

This is the documentation site for the [LEAP Encryption Access Project](https://leap.se).

Here you can find references to technical documentation and tutorials for different parts of the project.

[NOTE]
As of today, LEAP offers building blocks for provisioning a secure VPN service.
As an example, [RiseupVPN](https://riseup.net/en/vpn) is deployed used the LEAP VPN Platform, and the
clients are built from the leap-android and leap-desktop codebase.


[TIP]
This is a living index. More detailed documentation can usually be found within each repository.


## Platform

{{< columns >}}

### Overview :mag_right:

An architectural [overview]({{< ref "/overview" >}} "overview")  of the different components used in LEAP.


<--->

### Circumvention :fire:

A short introduction to common censorship categories, and some
[circumvention]({{< ref "/circumvention" >}}) techniques used by our tech.

<--->

### Tutorials :book:

A series of [tutorials]({{< ref "/tutorials" >}}) to help you getting started
with several tasks related to setting the LEAP VPN Platform.

<--->

{{< /columns >}}


## Clients

{{< columns >}}

### Android :iphone:

The android client is developed in
[leap/bitmask_android](https://0xacab.org/leap/bitmask_android). Go there to track issues, or learn how to contribute to android development.

<--->

### Desktop :computer:

Desktop application is developed in
[leap/bitmask-vpn](https://0xacab.org/leap/bitmask-vpn). Issues and hints for
developers live on that repo.


<--->

### l10n localization :globe_with_meridians:

Visit the [Localization Lab wiki](https://wiki.localizationlab.org/index.php/Bitmask) to translate our apps.
For these documentation pages, send pull-reqs to
[leap-docs](https://0xacab.org/leap/leap-docs) :black_heart:

<--->
{{< /columns >}}
