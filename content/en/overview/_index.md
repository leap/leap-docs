---
title: Overview
geekdocNav: true
geekdocAnchor: true
---

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD033 -->

<!-- markdownlint-restore -->

A high-level overview of all the moving parts in the LEAP Architecture.


{{< columns >}}

### Web Services

Two services coordinate credentials distribution, discovery and
resource assignment: [vpnweb](https://0xacab.org/leap/vpnweb) (client
certificates and static files) and [menshen](https://0xacab.org/leap/menshen)
(a geolocation service and load balancer that helps with gateway and bridge
selection).

<--->

### Orchestration

[Lilypad](https://0xacab.org/leap/container-platform/lilypad) is our
opinionated answer to the needs of deploying all the server-side
components. Get in touch if you're interested in using the building blocks in a
different way.

<--->

### API Reference

See the [developer documentation
repository](https://0xacab.org/leap/dev-documentation) for the server-side API,
and some design proposals under discussion.

{{< /columns >}}

