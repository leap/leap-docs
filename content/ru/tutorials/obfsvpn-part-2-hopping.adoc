+++
title = "Настройка обфусцированной службы VPN (2/2)"
+++

:toc:
:toclevels: 2

{{< toc >}}

[cols="2,2"]
|===
|*Автор*
|kikki

|*Последний раз редактировалось*
|25 Mar 2023
|===

== Автоматическое переключение между мостами (aka Hopping)

По ссылке link:{{< ref "/tutorials/obfsvpn-part-1" >}}[предыдущая часть этого туториала] мы настроили шлюз OpenVPN и один `obfs4` мост, который на него указывает, а также проверили, что наша конфигурация нормально работала.

Теперь давайте ещё больше запутаем цензоров, создав бесконечное переключение с одного моста на другой. Это новый подключаемый транспорт, который сейчас мы будем называть link:{{< ref "/circumvention/hopping">}}["Hopping PT"], но вы можете предложить свои варианты. В LEAP достаточно долго используется практика называть что-то так же, как животное, которое прыгает, так что тут, кажется, мы будем использовать слово _кузнечик_?

=== Донастроим шлюз

Нам нужно изменить конфиг `openvpn`, чтобы использовать UDP, и добавить парочку других штук:

Откройте shell в шлюзе:

```Shell
cd /etc/openvpn
cp server.conf server-hopping.conf
```

Измените `/etc/openvpn/server-hopping.conf`, чтоб он выглядел так:

{{< highlight vim "linenos=table" >}}
proto udp

push "ip link set mtu 48000 dev tun0"
float
tun-mtu 48000
fragment 0
mssfix 0

cipher AES-256-CBC
[...]
{{< /highlight >}}

=== Перезапустите шлюз OpenVPN с новыми настройками

* `systemctl stop openvpn@server.service`
* `systemctl start openvpn@server-hopping.service`

== Настроим второй мост

Сделайте новый 
`obfs4` мост, пользуясь инструкциями из первой части туториала. Используйте части до link:{{< ref "obfsvpn-part-1/#_start_the_bridge" >}}[запустим мост].

```Shell
./server -addr 0.0.0.0 -h -state test_data -c test_data/obfs4_state.json -v -vpn $RHOST
```

Для режима с переподключениями нам не нужно указывать порт, они генерируются рандомно.

=== Настроим ещё и первый мост

* Подсоединитесь к вашему первому мосту, а потом остановите его, если он запущен
* После этого используте ту же самую команду, что и выше, чтобы перезапустить его

=== Запустите obfsvpn клиент в режиме переподключения

Возвращаемся к машине с клиентом и чуть-чуть изменяем obfsvpn:

* Для начала, остановите клиент, нажав `ctrl+c` в терминале, в котором запущен клиент
* Перезапустите клиент с новыми аргументами: 

```Shell
./client -h -c <bridge1_cert>,<bridge2_cert> -r <bridge_ip_1>,<bridge_ip_2>
```

=== Настроим клиент OpenVPN

{{< highlight Bash >}}
sudo su
cd /etc/openvpn
cp client.conf client-hopping.conf
{{< /highlight >}}

Теперь измените client-hopping.conf, чтобы он выглядел так:

{{< highlight vim "linenos=table" >}}
proto udp 

replay-window 65535
tun-mtu 48000
fragment 0
mssfix 0

cipher AES-256-CBC
{{< /highlight >}}

=== Запускаем!

Режим переподключения сильно отличается, потому что нам не нужны прокси (Socks) между клиентами openvpn и  obfsvpn. 
Вместо этого мы подключаемся к клиенту obfsvpn через UDP, как будто этот клиент и есть сервер openvpn.

```Shell
openvpn --config client-hopping.conf --remote 127.0.0.1:8080
```

Надеюсь, инициализация туннелей прошла успешно. С точки зрения `openvpn` режим переподключения ничего не поменял.


Надеюсь, все закончилось успешной инициализацией туннеля. С точки зрения `openvpn`, использование режима hopping не должно иметь никакого значения.

:tip-caption: pass:[Поздравляем!]

[TIP]
Это всё на текущий момент! Теперь вы можете подключаться к собственным шлюзам OpenVPN и автоматически разделять трафик между двумя обфусицированными мостами **Успешного обхода блокировок! 🥳**
