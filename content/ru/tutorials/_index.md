---
title: Туториалы
geekdocNav: true
geekdocAnchor: true
---

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD033 -->

<!-- markdownlint-restore -->

Целенаправленные, пошаговые записи. Их цель — познакомить вас с разными "кирпичиками"
 платформы LEAP VPN, шаг за шагом направить вас к цели, а также объяснять, как
компоненты соединяются друг с другом.


## Настройка платформы

* [Lilypad Readme (english)](https://0xacab.org/leap/container-platform/lilypad/-/blob/main/README.md): здесь описаны основы настройки платформы.


## Обход цензуры

* [Настройка обфусцированного шлюза OpenVPN, часть 1]({{< ref "/tutorials/obfsvpn-part-1" >}}): здесь показано, как вручную настроить собственный обфусцированный шлюз с помощью [obfsvpn](https://0xacab.org/leap/obfsvpn) коммерчески доступного VPS.
* [Настройка обфусцированного шлюза OpenVPN, часть 2]({{< ref "/tutorials/obfsvpn-part-2-hopping" >}}): основываясь на предыдущей части, он включает [Переподключаемый транспорт]({{< ref "/circumvention/hopping" >}})

