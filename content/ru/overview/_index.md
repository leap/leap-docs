---
title: Общая информация
geekdocNav: true
geekdocAnchor: true
---

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD033 -->

<!-- markdownlint-restore -->

Высокоуровневый обзор всех частей архитектуры LEAP.


{{< columns >}}

### Веб-сервисы

Два сервиса отвечают за распределение кредов, 
обнаружение и назначение ресурсов: [vpnweb](https://0xacab.org/leap/vpnweb) (сертификаты клиентов
и статические файлы) и [menshen](https://0xacab.org/leap/menshen)
(сервис геолокации и балансировщик нагрузки, который
помогает выбирать шлюз и мосты).

<--->

### Оркестрация

[Lilypad](https://0xacab.org/leap/container-platform/lilypad) — это наш выбор, 
когда нам нужно задеплоить любые наши серверные компоненты. 
Свяжитесь с нами, если хотите использовать что-то другое.

<--->

### Справочник по API

Посмотрите страницу [developer documentation
repository](https://0xacab.org/leap/dev-documentation) для серверного API и чтобы
найти некоторые предложения по дизайну, которые пока что находятся на стадии обсуждения.

{{< /columns >}}

