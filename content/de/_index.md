+++
title = "Willkommen bei der LEAP Dokumentation!"
geekdocNav = true
geekdocAlign = "left"
geekdocAnchor = false
+++


Dies ist die Dokumentationsseite für das [LEAP Encryption Access Project](https://leap.se).

Hier findest Du Verweise auf technische Dokumentationen und Tutorials für verschiedene Teile des Projekts.

[NOTE]
LEAP bietet derzeit Bausteine für die Bereitstellung eines sicheren VPN-Dienstes an.
Zum Beispiel wird [RiseupVPN](https://riseup.net/en/vpn) auf der LEAP VPN-Plattform eingesetzt, und die Clients werden aus der Codebasis von Bitmask-Android und Bitmask-Desktop erstellt.


[TIP]
Dies ist ein lebendiger Index. Ausführlichere Dokumentationen sind in der Regel in den einzelnen Repositorys zu finden.


## Plattform

{{< columns >}}

### Overview :mag_right:

Ein architektonischer [Überblick]({{< ref "/overview" >}}) über die verschiedenen in LEAP verwendeten Komponenten.

<--->

### Umgehung :fire:

Eine kurze Einführung in gängige Zensurkategorien, und einige [Umgehungstechniken]({{< ref "/circumvention" >}}), die von unserer Technologie verwendet werden.

<--->

### Tutorials :book:

Eine Reihe von [Tutorials]({{< ref "/tutorials" >}}), um Ihnen den Einstieg zu erleichtern
mit verschiedenen Aufgaben im Zusammenhang mit der Einstellung der LEAP VPN Plattform.

<--->

{{< /columns >}}


## Clients

{{< columns >}}

### Android :iphone:

Der Android-Client wird entwickelt in [leap/bitmask_android](https://0xacab.org/leap/bitmask_android) entwickelt. Gehen Sie dorthin, um Fehler zu melden oder zu erfahren, wie Sie zur Android-Entwicklung beitragen können.

<--->

### Desktop :computer:

Die Desktop-Anwendung wird entwickelt in [leap/bitmask-vpn](https://0xacab.org/leap/bitmask-vpn). Probleme und Hinweise für Entwickler befinden sich in diesem Repo.


<--->

### l10n Lokalisierung :globe_with_meridians:

Besuchen Sie das [Localization Lab wiki](https://wiki.localizationlab.org/index.php/Bitmask), um unsere Anwendungen zu übersetzen.
Für diese Dokumentationsseiten senden Sie Pull-Requests an [leap-docs](https://0xacab.org/leap/leap-docs) :black_heart:

<--->
{{< /columns >}}
