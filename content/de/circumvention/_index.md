---
title: Umgehungstaktiken
geekdocNav: true
geekdocAnchor: true
---

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD033 -->

<!-- markdownlint-restore -->

Ein Überblick über Zensurtechniken und einige Umgehungstaktiken, die LEAP nutzt.

{{< toc >}}

## Einleitung

Wir verstehen, dass Du, wenn Du bereits ein VPN benutzt, über Überwachung und Zensur nachdenkst und wie diese beiden Dinge normalerweise zwei Seiten der gleichen Medaille sind. Die Electronic Frontier Foundation betreibt die die ausgezeichnete Website [Understanding and Circumventing Network Censorship](https://ssd.eff.org/module/understanding-and-circumventing-network-censorship) als Teil ihres Leitfadens zur Selbstverteidigung gegen Überwachung.

Auch wenn du bereits einiges über Zensur weißt, laden wir dich ein, dein Wissen mit dieser Quelle aufzufrischen! Um das sich ständig weiterentwickelnde Spiel der Zensur-Umgehung zu verstehen, ist es sehr hilfreich, ein gutes Gedankenmodell davon zu haben, wo die Sperrung von Internetseiten und Datenverkehr stattfinden kann, um Umgehungstechniken zu entwickeln - oder um zu verstehen, wie sie funktionieren, und wie man feststellen kann, wie sich die Situation mit der Zeit ändert.

Wenn Du Dich für tote Bäume oder ihr elektronisches Äquivalent interessierst, gibt es dieses kleine Buch von [ARTICLE 19] (https://www.article19.org/), das wir nicht genug loben können: [How the Internet Really Works - An Illustrated Guide to Protocols, Privacy, Censorship, and Governance] (https://nostarch.com/how-internet-really-works). 
Umgehung wird darin behandelt, und Sie bekommen eine umfassende Vorstellung davon, wie das Internet funktioniert (und es gibt auch einige Katzenbilder!).

## Verschiedene Formen der Internetstörung

### Wer kann das Internet zensieren?

Die Sperrung oder Filterung kann von Deinem **lokalen Netzwerkadministrator*innen** vorgenommen werden, oder von Deinem Internetdienstanbieter (**ISP**). Oft werden ISPs von Regierungen gezwungen, Sperrungen oder Filterungen vorzunehmen.

### Was ist das Ziel der Sperrung?

Sperren (von Websites) können auf vielen Ebenen erfolgen: zwei gängige Beispiele sind Sperren auf der Ebene der **IP-Adresse**, oder durch Eingriffe in das **DNS** (Domain Name System: das Ding, das es Dir erlaubt, lustige Namen in die Browserleiste einzugeben statt lange Zahlenreihen einzugeben - und dasselbe Ding, das Dich dazu zwingt, coole Domains für Dein neues Projekt oder Deine Kampagne zu kaufen).

Je nachdem, wie der Zugang zu den Inhalten beeinträchtigt wird, können wir mehrere Kategorien von Sperren unterscheiden:

### 1. Sperrung einer Website nach IP

Der Zugriff auf eine bestimmte IP-Adresse wird verweigert. Wenn mehrere Websites dieselbe IP-Adresse verwenden (z. B. wenn sie auf demselben Rechner gehostet werden), ist der Zugriff auf alle diese Websites nicht möglich.

### 2. Blockieren einer Website über DNS

Der Zugriff auf die Website wird verweigert, indem die DNS-Einträge (Domain Name System) so geändert werden, dass der Domänenname der Website nicht in ihre IP-Adresse aufgelöst wird.

### 3. Bandbreiten-Drosselung

Internetdienstanbieter können die Geschwindigkeit bestimmter Websites drosseln und so deren Nutzung erschweren.

 ### 4. Filterung nach Protokoll

Internetdienstanbieter blockieren bestimmte Arten von Internetverkehr, z. B. Dateidownloads, Anonymisierungsdienste und virtuelle private Netze, auf der Grundlage des verwendeten Protokolls, z. B. OpenVPN.

### 5. Filterung durch Deep Packet Inspection: 

Selbst verschlüsselter Datenverkehr kann anhand von Metaparametern wie der Paketlänge und dem unverschlüsselten Inhalt der Header (falls vorhanden) identifiziert werden. Als verdächtig eingestufte Verbindungen werden blockiert.

### 6. Entfernung von Inhalten

Die Regierung setzt die Eigentümer von Inhalten oder Plattformen unter Druck, Inhalte aus dem Netz zu entfernen. 

### 7. Unterbrechung des Netzes

Die Regierung unterbricht das normale Funktionieren des Internets. Dies kann Denial-of-Service-Angriffe (DoS) umfassen oder Internetanbieter zwingen, den Betrieb in bestimmten Gebieten vorübergehend einzustellen.

## Zensur im breiteren Kontext

Zensur ist ein komplexes Thema, das wir aus vielen Blickwinkeln betrachten können. [Access Now hat eine Klassifizierung der Internetabschaltungen](https://www.accessnow.org/publication/internet-shutdown-types/) ausgearbeitet, die
teilweise mit der von uns oben vorgeschlagenen Klassifizierung übereinstimmt. Demnach reichen Störungen des Internets von einem totalen Blackout (oder Shutdown) der Internet-Infrastruktur über Routing-Störungen, DNS Manipulation, selektivem Filtern, Deep Packet Inspection, Rogue Infrastructure und Denial of Service Angriffen bis hin zur Drosselung des Datenverkehrs.

## Wie hilft ein VPN bei der Zensur?

* Ein VPN ist in der Lage, "normale" Webseitensperren zu umgehen, weshalb sich ISPs oder Regierungen wiederum daran versuchen, auch VPNs aufzuspüren und zu blockieren. Das ist ein ziemlich mühsames Spiel.

* Ein VPN führt in der Regel ein paar Schritte aus, um eine Verbindung herzustellen, und alle diese Schritte sind sind potentielle Ziele, welche Regierungen und ISPs versuchen können zu blockieren - und wofür wir auch einige Gegenmaßnahmen haben.

* Regierungen oder ISPs versuchen im Allgemeinen, VPNs auf 4 Ebenen zu blockieren:
  1. **Vertriebsseiten** für die App (z. B. Schließung des App-Stores),
  2. Blockierung des Zugriffs zur **erstmaligen Erkennung** und Konfiguration der Gateways,
  3. Blockieren des Zugriffs auf die **VPN-Gateways** selbst;
  4. Keine der oben genannten Maßnahmen ergreifen, sondern die Verbindung zulassen und dann die **Verbindungsbandbreite stören**, bis zu dem Punkt, an dem alles schmerzhaft langsam ist und Nutzende (nicht Du!) müde, frustriert und gelangweilt sind und dann schwören, nie wieder ein VPN zu benutzen. Dies ist gemeinhin bekannt als "Drosselung" bekannt - und sie mit genügend empirischen Beweisen zu erkennen, erweist sich als eine besonders schwierige Frage.

* Einige der Versuche, den Zugang zum VPN (oder die VPN-Anbieterseite) zu "blockieren", sind leichter zu bekämpfen als andere.

* Einige VPN **Protokolle** sind leichter zu erkennen als andere. OpenVPN (ja, das verwenden wir für die regulären Gateways) ist  besonders leicht zu blockieren.

* Die gute Nachricht ist, dass es einige Protokolle gibt, von denen bekannt ist, dass sie gängigen  Erkennungstechniken widerstehen - zumindest für eine gewisse Zeit.

* Selbst wenn Sie Glück haben und einen VPN-Tunnel zum Laufen bringen, können die von Ihnen besuchten Websites auch erkennen, dass Sie ein VPN verwenden, und beschließen Ihnen ihre Inhalte nicht zu zeigen. Normalerweise tun sie dies, indem sie Dritte für Listen, die übliche IP-Bereiche bekannter VPN-Gateways enthalten, bezahlen. Dies ist ein Fall von  **Geofencing** (eine Art serverseitige Sperrung).


### 1. Wenn eine Website nach IP gefiltert (oder verlangsamt) wird

Wenn ein Benutzer mit LEAP VPN auf eine Website zugreift, kann der Internetdienstanbieter nicht sehen, auf welche bestimmte Website der Benutzer zugreift. Vom Standpunkt des ISP aus gesehen spricht der Computer des Benutzers mit einem fremden Rechner in einem anderen Land.

Bei **regulären Gateways** kann der ISP die Verwendung von OpenVPN erkennen, auch wenn die betreffende Website nicht sichtbar ist und versuchen, die VPN-Verbindung selbst zu drosseln oder zu blockieren.

Bei der Verwendung von **Brücken** ist der Tunnel bis zum Gateway verschleiert. Wir gehen davon aus, dass das VPN-Gateway ungefilterten Zugang zum Internet hat.


### 2. Wenn eine Website auf DNS-Ebene blockiert wird

LEAP VPN leitet alle DNS-Anfragen von Anwendungen über das VPN-Gateway. Da die Verbindung zum Gateway verschlüsselt ist, können DNS-Anfragen nicht vom vom ISP blockiert werden.

### 3. Filterung nach Protokoll und durch Deep Packet Inspection

ISPs können OpenVPN-Verbindungen blockieren und versuchen, Gateways aufzulisten, um sie nach IP zu blockieren.

Aus diesem Grund empfehlen wir die Verwendung von Brücken, um den  Zugang zu Gateways aus zensierten Umgebungen zu erleichtern.
Bridges verwenden häufiger das [obfs4-Protokoll](https://github.com/Yawning/obfs4/blob/master/doc/obfs4-spec.txt), um OpenVPN-Datenübertragungen zwischen dem Benutzer und einer Bridge zu verbergen.
Von außen sieht der obfs4-Tunnel wie ein Austausch von Zufallsrauschen aus, was theoretisch hilft, unter dem Radar der Zensoren durchzukommen (aber Datenverkehr mit hoher Entropie hat seine Probleme).

Wir arbeiten auch an nicht-öffentlichen Brücken, die schwieriger aufzulisten sein sollten, und daher weniger wahrscheinlich blockiert werden.

### 4. Entfernung von Inhalten und dienstbezogene Netzstörungen

Ein VPN kann bei diesen Problemen nicht wirklich helfen, aber es kann in Kombination mit anderen Werkzeugen genutzt werden. Man kann zensurresistente dezentralisierte Plattformen wie Fediverse verwenden, um weniger von der Entfernung von Inhalten betroffen zu sein. Für einen zuverlässigeren Internetdienst kann man Satelliteninternet, stationäre Mesh-Netzwerke wie Freifunk, sofern so etwas in deiner Gegen existiert, oder die Nutzung kleinerer Internetanbieter oder Kombinationen von Internetanbietern in Erwägung ziehen.

## Wie Sie Ihre VPN-Nutzung zensurresistenter machen

Es gibt ein paar Techniken, die mit LEAP VPN verwendet werden können, um einige der oben erwähnten Blockierungen zu umgehen. Einige dieser Techniken werden automatisch durchgeführt, bei anderen müssen Nutzende die Anwendungseinstellungen ändern.

### Während der Anfangsphase (Tunnel-Bootstrapping)

* Wenn wir die Anfangskonfiguration nicht abrufen können, versuchen LEAP VPN-Clients, sie mit Snowflake abzurufen (dazu muss Tor installiert sein).
* Der von den Desktop-Bibliotheken verwendete TLS-Fingerprint kann verraten, dass wir kein Browser sind. Wir nutzen uTLS, um zu simulieren, dass wir ein Browser sind (und um in der Masse der Netzwerkspuren weniger auffällig zu sein).

### Brücken verwenden, um sich mit dem OpenVPN-Gateway zu verbinden

Was wir **Brücken oder Bridges** nennen, sind verschleierte Proxys, die den Datenverkehr bis zum endgültigen Ziel am Gateway weiterleiten. Die Verwendung von Bridges kann verhindern, dass der Internetanbieter erkennt, dass der Benutzer einen VPN-Tunnel aufbaut.

Übrigens haben wir den Begriff "Brücke" von der Tor-Community übernommen, in der Hoffnung, dass die Verwendung einer bekannten Metapher erleichtert die gleiche Idee zu übertragen.

Wir verwenden zwei Pluggable Transports mit unterschiedlichen Konfigurationsoptionen:

    1. obfs4
    2. obfs4 im KCP-Modus
    3. obfs4 im Hopping-Modus. 

Auf der Seite [Hopping Transport]({{<ref "/circumvention/hopping" >}}) finden Sie weitere technische Informationen über die Funktionsweise des Hopping-Modus.

## Überwachung

Man muss kein Experte oder Akademiker*in sein, um einen Beitrag zur Zensurforschung beizutragen (ja, diese beiden Dinge sind normalerweise nicht automatisch gleichzusetzen). Tatsächlich werden einige der wertvollsten Erkenntnisse darüber, was verschiedene Zensoren verwenden und welche Umgehungstechniken noch praktikabel sind von anonymen Personen geteilt, die freiwillig ihre Zeit und ihre Fähigkeiten einsetzen, um unser kollektives Wissen zu erweitern.

Wenn dies etwas ist, das dich interessieren könnte, dann lies bitte sorgfältig die Seite über [VPN-Messungen mit dem Open Observatory for Network Interference]({{<ref "/circumvention/ooni" >}}).