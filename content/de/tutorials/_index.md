---
title: Tutorials
geekdocNav: true
geekdocAnchor: true
---

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD033 -->

<!-- markdownlint-restore -->

Zielgerichtete, schrittweise Anleitungen. Sie haben das Ziel, Dich mit den
mit den Bausteinen der LEAP VPN Plattform vertraut zu machen und dabei Schritt für Schritt zu erklären, wie die Komponenten miteinander verbunden sind.

## Einrichten der Plattform

* [Lilypad Readme (english)](https://0xacab.org/leap/container-platform/lilypad/-/blob/main/README.md): Hier werden die Grundlagen zur Einrichtung der Plattform behandelt.


## Umgehung der Zensur

* [Setting Up An Obfuscated OpenVPN Gateway, Part 1]({{< ref "/tutorials/obfsvpn-part-1" >}}): es zeigt, wie man manuell sein eigenen obfuskierten Gateway mit [obfsvpn](https://0xacab.org/leap/obfsvpn) auf einem gewöhnlichen VPS einrichtet.
* [Setting Up An Obfuscated OpenVPN Gateway, Part 2]({{< ref "/tutorials/obfsvpn-part-2-hopping" >}}): aufbauend auf dem vorigen Teil, wird der [Hopping Pluggable Transport]({{< ref "/circumvention/hopping" >}}) aktiviert.
