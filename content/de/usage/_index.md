---
title: Usage
geekdocNav: true
geekdocAnchor: false
---

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD033 -->

<!-- markdownlint-restore -->

Blah blah.

## Feature overview

{{< columns >}}

### Clean and simple design

Stay focused on exploring the content and don't get overwhelmed by a complex design.

<--->

### Light and mobile-friendly

The theme is powered by less than 1 MB and looks impressive on mobile devices as well as on a regular Desktop.

<--->

### Easy customization

The look and feel can be easily customized by CSS custom properties (variables), features can be adjusted by Hugo parameters.

{{< /columns >}}

{{< columns >}}

### Zero initial configuration

Getting started in minutes. The theme is shipped with a default configuration and works out of the box.

<--->

### Handy shortcodes

We included some (hopefully) useful custom shortcodes so you don't have to and can focus on writing amazing docs.

<--->

### Dark mode

Powerful dark mode that detects your system preferences or can be controlled by a toggle switch.

{{< /columns >}}
