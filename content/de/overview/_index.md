---
title: Überblick
geekdocNav: true
geekdocAnchor: true
---

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD033 -->

<!-- markdownlint-restore -->

Ein grober Überblick über alle wichtigen Bestandteile der LEAP-Architektur.


{{< columns >}}

### Webdienste

Zwei Dienste koordinieren die Verteilung von Zugangsberechtigungen, die Gateway-Suche und Ressourcenzuweisung: [vpnweb](https://0xacab.org/leap/vpnweb) (Client-Zertifikate und statische Dateien) und [menshen](https://0xacab.org/leap/menshen) (ein Geolocation-Dienst und Load Balancer, der bei der Auswahl von Gateways und Bridges hilft).

<--->

### Container Orchestrierung

[Lilypad](https://0xacab.org/leap/container-platform/lilypad) ist unsere Antwort auf die Anforderungen alle serverseitigen Komponenten zur Verfügung zu stellen. Kontaktieren Sie uns, wenn Sie daran interessiert sind, unsere Container anders zu deployen.

<--->

### API-Referenz

Siehe die [Entwicklerdokumentation](https://0xacab.org/leap/dev-documentation) für die serverseitige API und einige zur Diskussion stehende Designvorschläge.

{{< /columns >}}