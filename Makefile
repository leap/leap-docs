THEME_VERSION := v0.38.0
THEME := hugo-geekdoc
BASEDIR := .
THEMEDIR := $(BASEDIR)/themes

.PHONY: doc
doc: doc-build

.PHONY: update-theme
update-theme:
	mkdir -p $(THEMEDIR)/$(THEME)/ ; \
	curl -sSL "https://github.com/thegeeklab/$(THEME)/releases/download/${THEME_VERSION}/$(THEME).tar.gz" | tar -xz -C $(THEMEDIR)/$(THEME)/ --strip-components=1

.PHONY: doc-build
doc-build:
	cd $(BASEDIR); hugo

.PHONY: doc-live
doc-live:
	cd $(BASEDIR); hugo --environment production

.PHONY: clean
clean:
	rm -rf $(THEMEDIR) && \
	rm -rf $(BASEDIR)/public

